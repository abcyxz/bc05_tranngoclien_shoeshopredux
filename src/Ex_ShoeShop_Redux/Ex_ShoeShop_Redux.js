import React, { Component } from 'react'
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop_Redux extends Component {
  render() {
    return (
      <div>
    
        <div className='container'>
          <Cart/>
          <ListShoe />
          <DetailShoe />
        </div>
      </div>
    )
  }
}
