import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, CHANGE_DETAIL, GIAM_SO_LUONG, TANG_SO_LUONG } from "./redux/constant/shoeConstant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
             onClick={() => {
              this.props.handleDownCart(item)
            }}>-</button>
            {item.number}
            <button
            onClick={() => {
              this.props.handleUpCart(item)
            }}>+</button>
          </td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
    
  };
};
// dispatch
let mapDispatchToProps = (dispatch) => {
  return{
    handleBuyShoe:(shoe) => {
      let action= {
        type: ADD_TO_CART,
        payload: shoe,
      }
      dispatch(action)
    },
    handleChangeDetail: (shoe) => {
      let action = {
        type: CHANGE_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
    handleUpCart: (shoe) => {
      let action = {
        type: TANG_SO_LUONG,
        payload: shoe,
      };
      dispatch(action);
    },
    handleDownCart: (shoe) => {
      let action ={
        type: GIAM_SO_LUONG,
        payload: shoe,
      };
      dispatch(action);
    }
  }
};

export default connect(mapStateToProps,mapDispatchToProps)(Cart);
