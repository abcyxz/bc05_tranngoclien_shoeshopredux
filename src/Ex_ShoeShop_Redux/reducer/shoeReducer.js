import { dataShoe } from "./../dataShoe";
import { ADD_TO_CART, CHANGE_DETAIL, GIAM_SO_LUONG, TANG_SO_LUONG } from "../redux/constant/shoeConstant";

let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[1],
  cart: [],
};

export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = state.cart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        // th2: sp chưa có trong giỏ hàng
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        // th1 đã có
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_DETAIL: {
      state.detail = action.payload;
      return {...state};
    }
    case TANG_SO_LUONG: {
      let cloneCart = [...state.cart];
      let index = state.cart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      state.cart[index].number++

      return { ...state,  cart: cloneCart };
    }
    case GIAM_SO_LUONG: {
      let cloneCart = [...state.cart];
      let index = state.cart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      state.cart[index].number--

      return { ...state,  cart: cloneCart };
    }
    default:
      return state;
  }
};

